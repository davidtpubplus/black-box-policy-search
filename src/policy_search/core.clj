(ns policy-search.core
  (require ;[ctp.core :as ctp]
           ;lA[ctp-new.core :as ctp-new]
           [rockwalk.core :as rockwalk]
           [guess-who.core :as guess-who])
  (:gen-class))

(defn -main
  [& args]
  (prn "Main classes for this project are: ctp.core, rockwalk.core, and guess-who.core"))
