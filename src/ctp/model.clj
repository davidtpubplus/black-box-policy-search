(ns ctp.model
  (:require [ctp.dijkstra :refer [shortest-path]])
  (:use [anglican runtime emit]))

(defdist factor* [] []
  (sample [this] nil)
  (observe [this value] value))

(defdist delta [x] []
  (sample [this] x)
  (observe [this value] (if (= x value) 0.0 (/ -1.0 0.0))))

(defn argmax
  "returns the index of the max entry in a collection of [k v] pairs"
  [thing]
  (first (apply max-key second thing)))

(defn adjacent
  "returns a vector of nodes indices that are adjacent to u" 
  [graph u]
  (mapv first (get graph u)))

(defn edges
  "returns a sequence of direct edges in the graph"
  [graph]
  (mapcat (fn [u]
            (map (fn [v]
                   [u v])
                 (map first (get graph u))))
            (range (count graph))))

(defn remove-edge
  "removes an edge from the graph"
  [graph u v]
  (let [edges-u (filter (fn [[w & _]] 
                          (not= w v))
                        (get graph u))
        edges-v (filter (fn [[w & _]]
                          (not= w u))
                        (get graph v))]
    (assoc graph 
      u (vec edges-u)
      v (vec edges-v))))

(defn restrict
  "restricts the set of edges in the graph at node u to the subset vs"
  [graph u vs]
  (let [current-vs (into #{}  (mapv first (get graph u)))
        remove-vs (reduce disj current-vs vs)]
    (reduce (fn [graph v]
              (remove-edge graph u v))
            graph         
            remove-vs)))
            
(defn distance
  "returns the distance between u and v in a graph,
  or nil when nodes are not adjacent"
  [graph u v]
  (some (fn [[w d o]]
          (when (= v w) d))
        (get graph u)))
            
(defn open-prob
  "returns the probability that a edge u v is open,
  or nil when edge probabilities are not specified"
  [graph u v]
  (some (fn [[w d o]]
          (when (= v w) o))
        (get graph u)))

(defn sample-weather
  "samples a sub-graph based on edge open/blocked probabilities"
  [graph base-prob]
  (loop [graph graph
         u 0 
         sub-graph []
         weather {}]
    (if-let [cs (first graph)]
      (let [vs (map first cs)
            es (map (partial conj #{u}) vs)
            ws (map (fn [v] 
                      (get weather 
                           #{u v} 
                           (sample (flip (* base-prob
                                            (or (open-prob graph u v) 
                                                1.0))))))
                    vs)]
        (recur (rest graph)
               (inc u)
               (conj sub-graph
                     (vec (keep (fn [[c w]]
                                  (when w c))
                                (map vector cs ws))))
               (merge weather (zipmap es ws))))
      sub-graph)))

(defn sample-good-weather
  "samples a traversable sub-graph based on edge open/blocked probabilities"
  [graph base-prob start target]
  (let [sub-graph (sample-weather graph base-prob)
        [path dist] (shortest-path sub-graph start target)]
    (if (not (empty? path))
      sub-graph
      (sample-good-weather graph base-prob start target))))

(with-primitive-procedures [factor*]
  (defm factor [log-weight]
    (observe (factor*) log-weight)))

(with-primitive-procedures [argmax edges]
  (defm random-policy 
    "Sample from open edges uniformly at random"
    [& _]
    (fn policy [u vs]
      (sample (categorical 
               (map vector 
                    vs 
                    (repeat (count vs) 1.))))))

  (defm edge-policy
    "Sample a utility for each edge from a gamma distribution. 
    Select according to maximum utility." 
    [& _]
    (let [utility (mem (fn [u v]
                         (sample [u v] 
                          (tag :policy
                           (gamma 1. 1.)))))]
      (fn policy [u vs]
        (argmax (map (fn [v] 
                       [v (utility u v)])
                     vs)))))

  (defm eager-edge-policy
    "Sample a utility for each edge from a gamma distribution. 
    Select according to maximum utility." 
    [instance-graph & _]
    (let [utility (reduce (fn [utility [u v]]
                            (assoc utility
                              [u v] (sample 
                                     [u v] 
                                     (tag :policy
                                          (gamma 1. 1.)))))
                          {}
                          (edges instance-graph))]
      (fn policy [u vs]
        (argmax (map (fn [v] 
                       [v (get utility [u v])])
                     vs)))))

  (defm node-policy
    "Sample a utility for each node from a gamma distribution.
    Select according to maximum utility"
    [& _]
    (let [utility (mem (fn [u]
                         (sample u
                          (tag :policy
                           (gamma 1. 1.)))))]
      (fn policy [u vs]
        (argmax (map (fn [v] 
                       [v (utility u)])
                     vs))))))

(with-primitive-procedures [shortest-path restrict]
  (defm optimistic-policy
    [instance-graph s t & args]
    (store :graph-state instance-graph)
    (fn policy [u vs]
      (let [graph-state (restrict (retrieve :graph-state)
                                  u vs)
            [path dist] (shortest-path graph-state u t)
            v (first path)]
        (store :graph-state graph-state)
        (first path)))))

(with-primitive-procedures [adjacent distance]
  (defm dfs-agent
    "simulates travel using depth-first search, choosing
    from open edges according to a specified policy"
    [graph start target policy]
    (loop [path [start]
           counts {}
           dist 0.0]
      (let [u (peek path)]
        (if (= u target)
          ;; reached goal state
          [path dist counts]
          (let [unvisited (filter 
                           (fn [v] (not (get counts #{u v})))
                           (adjacent graph u))]
            (if (empty? unvisited)
              (if (empty? (pop path))
                ;; graph is not traversable
                [nil dist counts]
                ;; return to previous node
                (let [v (peek (pop path))]
                  (recur (pop path)
                         (assoc counts #{u v} 2) 
                         (+ dist (distance graph u v))))) 
              ;; choose node according to policy
              (let [v (policy u unvisited)]
                (if v
                  ;; move to new node
                  (recur (conj path v)
                         (assoc counts #{u v} 1)
                         (+ dist (distance graph u v)))
                  ;; policy choice is to back track
                  (let [v (peek (pop path))]
                    (if v
                      ;; return to previous node
                      (recur (pop path)
                             (assoc counts #{u v} 2) 
                             (+ dist (distance graph u v)))
                      ;; graph is not traversable
                      [nil dist counts])))))))))))

(with-primitive-procedures [shortest-path]
  (defm clairvoyant-agent
    "simulates travel along the graph by a clairvoyant agent, that 
    knows which edges exist in advance, and can therefore follow the
    shortest path"
    [graph start target _]
    (let [[path dist] (shortest-path graph start target)
          counts (when (seq path)
                   (into {} (map (fn [u v] [#{u v} 1])
                                 (butlast path)
                                 (rest path))))]
      [path dist counts]))
  
  (defm random-agent
    "simulates travel along graph selecting edges at random"
    [graph start target _]
    (dfs-agent graph start target (random-policy))))

(with-primitive-procedures [sample-good-weather shortest-path delta]
  (defquery ctp
    "policy learning for the canadian traveler problem"
    [problem base-prob simulate-agent make-policy]
    (let [graph (get problem :graph)
          s (get problem :s)
          t (get problem :t)
          ;; sample a problem instance with open/blocked edges
          sub-graph (sample-good-weather graph base-prob s t)
          ;; initialize a policy
          policy (when make-policy (make-policy graph s t))
          ;; attempt to traverse graph
          [path dist counts] (simulate-agent sub-graph s t policy)]
      ;; assign weight according to traveled distance
      (factor (- dist))
      ;; return found path, traveled distance and edge counts
      (predict :path path)
      (predict :distance dist)
      (predict :counts counts))))
  
