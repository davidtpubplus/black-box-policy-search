(ns rockwalk.core
  (:gen-class)
  (:require [clojure.tools.cli :as cli]
            [clojure.data.json :as json]
            [anglican
             [state :refer [get-predicts]]
             [core :refer [doquery]]
             [bbvb :refer [get-variational]]]
            [rockwalk
             [model :refer [rockwalk]]
             [data :refer [instances]]])
  (:use [anglican runtime emit]))


(def +instance-id+ :4x4)
(def +numsteps-range+ [1 2 5 10 20 50 100 200 500 1000])
(def +hed+ 10.0)
(def +num-test-episodes+ 1000)

(def +number-of-particles+ 1000)
(def +base-stepsize+ 0.1)
(def +adagrad+ 0.9)
(def +robbins-monro+ 0.5)

(defn export-instances []
  (spit "resources/rockwalk/instances.json"
      (json/write-str
       (into {}
             (map (fn [[id instance]]
                    [id (assoc instance
                          :rocks (into {}
                                       (map (fn [[loc _]]
                                              [loc loc])
                                            (:rocks instance))))])
                  instances)))))

(defn test-proposals
  "tests learned proposals by simulating
  a specified number of episodes"
  [proposals instance hed num-episodes]
  (let [;; generate i.i.d. samples of episodes
        ;; note: there are no gradient updates until
        ;; after the first num-episodes samples
        samples (->> #(first (doquery :bbeb rockwalk
                                      [instance hed 1.0]
                                      :only [:policy]
                                      :adagrad false
                                      :number-of-particles 1
                                      :initial-proposals proposals
                                      :stripdown false))
                     (repeatedly num-episodes))
        rewards (map (comp :reward get-predicts)
                     samples)
        visited (map (comp :visited get-predicts)
                     samples)
        rock-counts (frequencies (reduce concat visited))
        travel-counts (reduce
                       (fn [counts rocks]
                         (let [start [(:x instance) (:y instance)]
                               ;; append start node to sequenc of visited rocks
                               us (conj (seq rocks) start)
                               ;; append end node if last rock was not on edge
                               vs (if (= (first (last rocks)) (:n instance))
                                    rocks
                                    (conj (vec rocks)
                                          [(:n instance)
                                           (second (or (last rocks) start))]))]
                           (reduce
                            (fn [counts edge]
                              (update-in counts [edge] (fnil inc 0)))
                            counts
                            (map vector us vs))))
                       {}
                       visited)]
    [rewards rock-counts travel-counts]))

(defn trial
  [instance hed num-train-steps num-test-episodes
   & {number-of-particles :number-of-particles
      base-stepsize :base-stepsize
      adagrad :adagrad
      robbins-monro :robbins-monro
      :or {number-of-particles +number-of-particles+
           base-stepsize +base-stepsize+
           adagrad +adagrad+
           robbins-monro +robbins-monro+}}]
  (let [state (->> (doquery :bbeb rockwalk
                            [instance hed 1.0]
                            :only [:policy]
                            :number-of-particles number-of-particles
                            :base-stepsize base-stepsize
                            :adagrad adagrad
                            :robbins-monro robbins-monro
                            :stripdown false)
                   (drop (* num-train-steps number-of-particles))
                   first)
        proposals (get-variational state)]
    (conj (test-proposals proposals
                          instance
                          hed
                          num-test-episodes)
          (zipmap (map (comp vec rest) (keys proposals))
                  (map second (vals proposals))))))

(defn run-trials
  [& {instance-id :instance-id
      numsteps-range :numsteps-range
      num-test-episodes :num-test-episodes
      number-of-particles :number-of-particles
      base-stepsize :base-stepsize
      adagrad :adagrad
      robbins-monro :robbins-monro
      :or {instance-id +instance-id+
           numsteps-range +numsteps-range+
           num-test-episodes +num-test-episodes+
           number-of-particles +number-of-particles+
           base-stepsize +base-stepsize+
           adagrad +adagrad+
           robbins-monro +robbins-monro+}}]
  (doseq [num-steps numsteps-range]
    (let [[rewards rock-counts travel-counts policy]
          (trial (instances instance-id)
                 +hed+
                 num-steps
                 num-test-episodes
                 :number-of-particles number-of-particles
                 :base-stepsize base-stepsize
                 :adagrad adagrad
                 :robbins-monro robbins-monro)
          file-name (format "results/rockwalk/rockwalk-%s-d%.1f-rho%.1f-gamma%.1f-kappa%.1f-steps%05d-test%d.json"
                            (name instance-id)
                            +hed+
                            base-stepsize
                            adagrad
                            robbins-monro
                            num-steps
                            num-test-episodes)]
      (println (format "writing: %s" file-name))
      (spit file-name
            (json/write-str {:rewards rewards
                             :rock-counts (into [] rock-counts)
                             :travel-counts (into [] travel-counts)
                             :policy (into [] policy)
                             :training-parameters {:number-of-particles number-of-particles
                                                   :number-of-steps num-steps
                                                   :base-stepsize base-stepsize
                                                   :adagrad adagrad
                                                   :robbins-monro robbins-monro}
                             :num-test-episodes num-test-episodes})))))

(def cli-options
  [["-i" "--instance-id NAME" "Instance name"
    :parse-fn keyword
    :default :4x4
    :validate [instances "instance does not exist"]]

   ["-t" "--num-test-episodes NUMBER" "Number of test sweeps"
    :parse-fn #(Integer/parseInt %)
    :default +num-test-episodes+]

   ["-r" "--numsteps-range RANGE" "Range of numbers of steps"
    :parse-fn #(read-string (str "[" % "]"))
    :default +numsteps-range+]

   ["-a" "--adagrad GAMMA" "Decay rate for AdaGrad/RMSProp"
    :parse-fn #(Double/parseDouble %)
    :default +adagrad+]

   ["-k" "--robbins-monro KAPPA" "Decay rate for step size"
    :parse-fn #(Double/parseDouble %)
    :default +robbins-monro+]

   ["-h" "--help" "Print usage summary and exit"]])

(defn -main
  [& args]
  (let [{:keys [options arguments errors summary]
         :as parsed-options}
        (cli/parse-opts args cli-options)]
    (cond
     (:help options) (println summary)
     errors (println errors)
     (seq arguments) (println summary)

     :else
     (let [{:keys [instance-id
		   num-test-episodes numsteps-range
                   adagrad robbins-monro]}
           options]
       (println options)
       (run-trials :instance-id instance-id
		   :numsteps-range numsteps-range
                   :num-test-episodes num-test-episodes
                   :adagrad adagrad
                   :robbins-monro robbins-monro)))))