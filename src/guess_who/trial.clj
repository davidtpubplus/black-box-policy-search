(ns guess-who.trial
  "Guess Who POMDP solving with BBVB policy search"
  (:require [clojure.core.matrix :as mat :refer [array]]
            [guess-who
             [data :refer [+questions+ +entities+]]
             [heuristics :refer [myopic-voi recursive-voi]]
             [model :refer [+reliability+ posterior-belief response-probability 
                            relative-utility update-info max-entries]]
             [policy :refer :all]]
            [anglican
             [trap :refer [value-cont]]
             [state :refer [initial-state]]
             [bbvb :refer [get-variational]]
             bbeb])
  (:use [anglican runtime emit
         [state :only [get-predicts]]
         [core :only [doquery]]]))

(defn value-state-cont
  "returns both value and state"
  [v s]
  [v s])

(with-primitive-procedures
  [update-info posterior-belief response-probability
   relative-utility max-index array make-policy select]

  (defm sample-policy
    "samples policy parameters and returns a policy instance of specified type"
    [policy-type]
    (let [sample-weights (fn [] (array
                                  (map (fn [q]
                                         (map (fn [e]
                                                (sample [q e]
                                                 (tag :policy
                                                  (gamma 100.0 100.0))))
                                              (keys +entities+)))
                                       +questions+)))
          params (case policy-type
                   :linear-belief
                   (let [weights (sample-weights)]
                     [weights])

                   :discounted-belief
                   (let [weights (sample-weights)
                         gamma (sample :gamma 
                                 (tag :policy (beta 1 1)))]
                     [weights gamma])

                   :logistic-info
                   (let [weights (array
                                  (map (fn [q1]
                                         (flatten
                                          (map (fn [q2]
                                                 [(sample [:weights q1 q2 true]
                                                    (tag :policy
                                                      (gamma 10.0 100.0)))
                                                  (sample [:weights q1 q2 false]
                                                    (tag :policy
                                                      (gamma 10.0 100.0)))])
                                               +questions+)))
                                       +questions+))
                         bias (array
                                (map (fn [q]
                                       (sample [:bias q]
                                         (tag :policy
                                           (normal 1.0 0.2))))
                                     +questions+))]
                     [weights bias])
                   nil)]
      (make-policy policy-type params)))

  (defquery simulate-episode
    "simulates an episode of guess who, sampling responses
    based on the current belief"
    [policy-type depth inverse-temp initial-info]
    (let [initial-info (or initial-info {})
          initial-belief (posterior-belief initial-info)
          inverse-temp (or inverse-temp 1.0)
          policy (sample-policy policy-type)]
      (loop [questions []
             info initial-info
             belief initial-belief
             reward 0.0]
        (if (>= (count questions) depth)
          (do
            (predict :policy policy)
            (predict :questions questions)
            (predict :reward reward)
            (predict :info info))
          (let [;; select question according to sampled policy
                question (select policy info)
                ;; simulate response according to marginal
                ;; probability given current belief
                response (sample
                          (flip (response-probability belief question)))
                ;; update information and belief state
                new-info (update-info info question response)
                new-belief (posterior-belief new-info)
                ;; update reward
                new-reward (relative-utility initial-belief new-belief)]
            ;; factor according to change in reward
            (observe (flip (exp (* inverse-temp (- new-reward reward)))) true)
            ;; continue to next question
            (recur (conj questions question)
                   new-info
                   new-belief
                   new-reward)))))))

(defn learn-policies
  "learns a policy using BBEB inference. returns empirical
  distribution of policies from last iteration"
  [policy-type depth number-of-steps
   & {initial-proposals :initial-proposals
      number-of-particles :number-of-particles
      base-stepsize :base-stepsize
      adagrad :adagrad
      robbins-monro :robbins-monro
      :or {:number-of-particles 100
           :base-stepsize 1.0
           :adagrad 0.9
           :robbins-monro 0.9}}]
  (let [samples (->> (doquery :bbeb
                              simulate-episode
                              [policy-type depth]
                              :only [:policy]
                              :initial-proposals initial-proposals
                              :number-of-particles number-of-particles
                              :base-stepsize base-stepsize
                              :adagrad true
                              :initial-proposals initial-proposals
                              :stripdown false)
                     (drop (* number-of-steps number-of-particles))
                     (take number-of-particles))
        proposals (get-variational (first samples))
        policies (map (comp :policy get-predicts)
                      samples)]
    [policies proposals]))

(defn test-episode
  "plays a guess who episode with fixed policy
  and returns the final reward"
  [true-id policy number-of-questions]
  (loop [number-of-questions number-of-questions
         info {}]
    (if (zero? number-of-questions)
      (let [belief (posterior-belief info)
            guess-id (rand-nth (max-entries belief))]
        (if (= guess-id true-id)
          1.
          0.))
      (let [question (select policy info)
            [attr value] question
            response (if (sample (flip +reliability+))
                       (= (get-in +entities+ [true-id attr]) value)
                       (not= (get-in +entities+ [true-id attr]) value))]
        (recur (dec number-of-questions)
               (update-info info question response))))))

(defn test-sweep
  "runs a test episode for each entity multiple
  times and returns the total reward"
  [policy number-of-questions number-of-sweeps]
  (reduce
   (fn [reward id]
     (+ reward
        (test-episode id
                      policy
                      number-of-questions)))
   0.0
   (reduce concat
           (repeat number-of-sweeps
                   (keys +entities+)))))

(defn trial
  "learns a policy and runs a number of test sweeps. returns
  the rewards for all test episodes and the learned policy."
  [policy-type number-of-questions
   number-of-particles number-of-steps number-of-test-sweeps
   & {base-stepsize :base-stepsize
      adagrad :adagrad
      robbins-monro :robbins-monro
      initial-proposals :initial-proposals
      :or {:base-stepsize 1.0
           :adagrad 0.9
           :robbins-monro 0.9}}]
  (let [[policies proposals] (learn-policies policy-type number-of-questions number-of-steps
                                             :number-of-particles number-of-particles
                                             :base-stepsize base-stepsize
                                             :adagrad adagrad
                                             :robbins-monro robbins-monro)
        rewards (doall (map #(test-sweep % number-of-questions number-of-test-sweeps)
                            policies))]
    [rewards policies proposals]))