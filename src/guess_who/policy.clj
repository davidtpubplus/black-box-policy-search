(ns guess-who.policy
  "Policy implementations for Guess Who"
  (:require [clojure.core.matrix :as mat
             :refer [add sub div mul mmul array]]
            [guess-who
             [data :refer [+questions+ +entities+]]
             [model :refer [posterior-belief max-entries]]
             [heuristics :refer [myopic-voi recursive-voi]]])
  (:use [anglican runtime emit
         [state :only [get-predicts]]
         [core :only [doquery]]]))

(mat/set-current-implementation :vectorz)

(defn max-index
  "returns index of element with maximal value"
  [coll]
  (loop [coll coll
         i 0
         i-max nil
         v-max nil]
    (if-let [v (first coll)]
      (if (and v-max (<= v v-max))
        (recur (rest coll) (inc i) i-max v-max)
        (recur (rest coll) (inc i) i v))
      i-max)))

(defn sigmoid
  "vectorized sigmoid transform y = 1 / (1 + exp(-x))"
  [x]
  (div 1.0 (add 1.0 (mat/exp (mul -1.0 x)))))

(defprotocol Policy
  (select [self info]))

;; select questions at random
(defrecord UniformPolicy
  []
  Policy
  (select
   [self _]
   (rand-nth +questions+)))

;; select questions according to highest myopic value of information
(defrecord MyopicVoiPolicy
  []
  Policy
  (select
   [self info]
   (let [value-estimate (map vector
                             +questions+
                             (map (partial myopic-voi info)
                                  +questions+))
         best-questions (max-entries value-estimate)]
     (rand-nth best-questions))))

;; multiplies the belief vector with a weight matrix and
;; selects the question according to the max entry of the
;; resulting vector
(defrecord LinearBeliefPolicy
  [weights]
  Policy
  (select
   [self info]
  (let [belief (mapv second (posterior-belief info))
        values (mmul weights belief)]
    (get +questions+
         (sample (discrete (mat/to-nested-vectors values)))))))

;; like linear policy but also discounts weights of previously
;; asked questions
(defrecord DiscountedBeliefPolicy
  [weights gamma]
  Policy
  (select
    [self info]
    (let [qcounts (map (fn [q] (apply + (info q))) +questions+)
          discounts (mapv #(Math/pow gamma %) qcounts)
          belief (mapv second (posterior-belief info))
          values (mul discounts (mmul weights belief))]
      (get +questions+
           (max-index values)))))

;; applies logistic regression transform to vectorized
;; information state, and returns the max index of the
;; resulting vector
(defrecord LogisticInfoPolicy
  [weights bias]
  Policy
  (select
   [self info]
   (let [info-vector (mapcat (fn [q]
                                (get info q [0.0 0.0]))
                              +questions+)
         values (sigmoid (add (mmul weights
                                    info-vector)
                              bias))]
     (get +questions+
          (sample (discrete (mat/to-nested-vectors values)))))))

(defn make-policy [policy-type parameters]
  (case policy-type
    :uniform (->UniformPolicy)
    :myopic-voi (->MyopicVoiPolicy)
    :linear-belief (apply ->LinearBeliefPolicy parameters)
    :discounted-belief (apply ->DiscountedBeliefPolicy parameters)
    :logistic-info (apply ->LogisticInfoPolicy parameters)
    (throw (Exception. "policy-type must be one of [:uniform :myopic-voi :linear-belief :discounted-belief :logistic-info]"))))
