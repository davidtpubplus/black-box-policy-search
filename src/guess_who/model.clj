(ns guess-who.model
  (:require [guess-who
             [data :refer [+entities+ +questions+]]]
            [anglican
             [runtime :refer [log exp log-sum-exp]]]))

;; GUESS WHO
;;
;; entities {id {attr value}}
;; questions [[attr value]]
;; info {[attr value] [num-true num-false]}
;; belief [[id prob]]

(def +reliability+ 0.9)

(defn update-info
  "incorporates a question response into the information state"
  [info question response]
  (let [[a b] (get info question [0 0])]
    (assoc info
      question (if response
              [(inc a) b]
              [a (inc b)]))))

(defn log-likelihood
  "returns log p(info | entity), the joint log probability
  of responses given the attribute values of an entity"
  [info entity]
  (reduce
    +
    0.0
    (map (fn [[[attr value] [a b]]]
		   (let [;; get probability of true response
                 pi (if (= (entity attr) value)
                       +reliability+
                       (- 1 +reliability+))]
             ;; binomial probabilities of responses
             (+ (* (log pi) a) (* (log (- 1 pi)) b))))
         info)))

(defn posterior-belief
  "returns p(id | info) the normalized posterior belief"
  [info]
  (let [log-ws (map (partial log-likelihood info)
                    (vals +entities+))
        log-sum-w (reduce log-sum-exp log-ws)]
    (map vector
         (keys +entities+)
         (map #(exp (- % log-sum-w))
              log-ws))))

(defn response-probability
  "returns p(response | belief), the marginal question response
  probability given current belief"
  [belief question]
  (let [;; calculate prior probability that question is true
        ;; (given belief)
        [attr value] question
        [a b] (reduce
                (fn [[a b] [id prob]]
                  (if (= (get-in +entities+ [id attr]) value)
                    [(+ a prob) b]
                    [a (+ b prob)]))
                [0.0 0.0]
        		belief)
        pi (/ a (+ a b))]
    ;; probability of `true` response given belief
    (+ (* +reliability+ pi)
       (* (- 1 +reliability+) (- 1 pi)))))

(defn max-entries
  "returns the list [k] of k with highest value v given a list of pairs [[k v]]"
  [pairs]
  (let [max-v (reduce max (map second pairs))]
    (keep (fn [[k v]]
            (when (= v max-v) k))
          pairs)))

(defn relative-utility [initial-belief final-belief]
  "returns the expected change in reward of a final belief state
  relative to an initial belief state"
  (let [initial-candidates (max-entries initial-belief)
        final-belief (into {} final-belief)]
    (- (reduce max (vals final-belief))
       (/ (reduce + (map final-belief initial-candidates))
          (count initial-candidates)))))
